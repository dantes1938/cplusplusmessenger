#include "widgettorrent.h"
#include "ui_widgettorrent.h"
#include "QDebug"
#include "QIcon"

WidgetTorrent::WidgetTorrent(int index, std::shared_ptr<Torrent> torrent_ptr, QString torrent_file_path_, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetTorrent),
    index(index),
    torrent(torrent_ptr),
    torrentFilePath(torrent_file_path_)
{
    ui->setupUi(this);
    //this->resize(WIDGET_WIDTH, this->height());
    this->resize(300, 300);
    
    dialogTorrent = new DialogTorrent(torrent);
    connect(dialogTorrent, &DialogTorrent::signalOpen, this, &WidgetTorrent::slotOpen);
    connect(dialogTorrent, &DialogTorrent::signalShowInFolder, this, &WidgetTorrent::slotShowInFolder);
    connect(dialogTorrent, &DialogTorrent::signalCopyMagnet, this, &WidgetTorrent::slotCopyMagnet);
    connect(dialogTorrent, &DialogTorrent::signalPlace, this, &WidgetTorrent::slotPlace);
    connect(dialogTorrent, &DialogTorrent::signalSharingSpeed, this, &WidgetTorrent::slotSharingSpeed);
    connect(dialogTorrent, &DialogTorrent::signalReceivingSpeed, this, &WidgetTorrent::slotReceivingSpeed);
    connect(dialogTorrent, &DialogTorrent::signalDelete, this, &WidgetTorrent::slotDelete);
    connect(ui->startStopButton, SLOT(clicked()), this, SLOT(pauseClicked()));
}


void WidgetTorrent::resizes() {
    this->resize(1000, 1000);
}

void WidgetTorrent::setPauseIcon(themes theme_) {
    switch (theme_) 
    {
    case themes::light:
        ui->startStopButton->setIcon(QIcon(":/light/image/light/light-pause-icon.ico"));
        break;
    case themes::dark:
        ui->startStopButton->setIcon(QIcon(":/dark/image/dark/dark-pause-icon.ico"));
        break;
    case themes::green:
        ui->startStopButton->setIcon(QIcon(":/green/image/green/green-pause-icon.ico"));
        break;
    case themes::red:
        ui->startStopButton->setIcon(QIcon(":/red/image/red/red-pause-icon.ico"));
        break;
    case themes::blue:
        ui->startStopButton->setIcon(QIcon(":/blue/image/blue/blue-pause-icon.ico"));
        break;
    }
}
void WidgetTorrent::setStartIcon(themes theme_)
{
    switch (theme_)
    {
    case themes::light:
        ui->startStopButton->setIcon(QIcon(":/light/image/light/light-play-icon.ico"));
        break;
    case themes::dark:
        ui->startStopButton->setIcon(QIcon(":/dark/image/dark/dark-play-icon.ico"));
        break;
    case themes::green:
        ui->startStopButton->setIcon(QIcon(":/green/image/green/green-play-icon.ico"));
        break;
    case themes::red:
        ui->startStopButton->setIcon(QIcon(":/red/image/red/red-play-icon.ico"));
        break;
    case themes::blue:
        ui->startStopButton->setIcon(QIcon(":/blue/image/blue/blue-play-icon.ico"));
        break;
    }
}
void WidgetTorrent::setOptionsIcon(themes theme_)
{
    switch (theme_)
    {
    case themes::light:
        ui->settingsButton->setIcon(QIcon(":/light/image/light/light-dots-icon.png"));
        break;
    case themes::dark:
        ui->settingsButton->setIcon(QIcon(":/dark/image/dark/dark-dots-icon.png"));
        break;
    case themes::green:
        ui->settingsButton->setIcon(QIcon(":/green/image/green/green-dots-icon.png"));
        break;
    case themes::red:
        ui->settingsButton->setIcon(QIcon(":/red/image/red/red-dots-icon.png"));
        break;
    case themes::blue:
        ui->settingsButton->setIcon(QIcon(":/blue/image/blue/blue-dots-icon.png"));
        break;
    }
}
void WidgetTorrent::setIcons()
{
    setOptionsIcon(widget_theme);
    setStartPauseIcon();
}

void WidgetTorrent::setStartPauseIcon() {
    if (torrent->GetHandle().is_paused())
    {
        setStartIcon(widget_theme);
        return;
    }
    setPauseIcon(widget_theme);
}

void WidgetTorrent::setTheme(themes theme_) 
{
    widget_theme = theme_;
    switch (widget_theme) {
    case themes::light:
        ui->frame->setStyleSheet("background-color: rgb(198, 198, 198);");
        break;
    case themes::dark:
        ui->frame->setStyleSheet("background-color: rgb(52, 52, 52);");
        break;
    case themes::green:
        ui->frame->setStyleSheet("background-color: rgb(43, 156, 62);");
        break;
    case themes::red:
        ui->frame->setStyleSheet("background-color: rgb(210, 33, 45);");
        break;
    case themes::blue:
        ui->frame->setStyleSheet("background-color: rgb(0, 12, 181);");
        break;
    }
    setIcons();
    
}



WidgetTorrent::~WidgetTorrent()
{
    delete ui;
}

void WidgetTorrent::pauseClicked()
{
    
    //this->setStatus(TorrentStatus::paused);
}

void WidgetTorrent::setTorrentName(QString path)
{
    QString name = QString::fromStdString(torrent->GetHandle().status().name);
    if (name.count() > 12)
        name = name.left(12) + "...";
    while (name.count() < 15)
        name += " ";
    /*
    if (path.right(8) == QString(".torrent")) {
        path = path.left(path.count() - 8);
    }
    qDebug() << path;
    if (path.contains('/') || path.contains('\\')) {
        for (int i = path.count() - 1; i >= 0; i--)
        {
            if (path[i] == '/' || path[i] == '\\')
            {
                name = (path.right(path.count() - 1 - i)).left(10);
                if (path.count() - 1 - i > 10)
                    name += QString("...");
                break;
            }
        }
    }*/
    ui->labelTorrentNum->setText(name);
}

void WidgetTorrent::setSize(unsigned long long size)
{
    this->size = size;
    ui->labelProgress->setText(QString::number(percent) + "% из " + convert(size));
    lt::session ses;
}

void WidgetTorrent::setProgress()
{
    int64_t total_buf = torrent->GetStats().GetTotal();
    if (total_buf > 0 && allTimeLoaded == -1) {
        allTimeLoaded = total_buf;
    }
    else if (total_buf > allTimeLoaded) {
        allTimeLoaded = total_buf;
    }
    QString overall;
    if (allTimeLoaded == -1) {
        overall = "...";
    }
    else {
        overall = convert(allTimeLoaded);
    }
    if (allTimeLoaded != -1) {
        percent = static_cast<double_t>(torrent->GetStats().GetTotalDone()) / static_cast<double_t>(allTimeLoaded) * 100;
    }
    if (status == libtorrent::torrent_status::seeding)
        percent = 100.0;

    if (percent < 0.0)
        percent = 0.0;
    auto numberPercent = qFloor(percent);
    if (numberPercent < 0)
        numberPercent = 0;
    ui->labelProgress->setText(QString::number(numberPercent) + "% из " + overall);
    ui->progressBar->setValue(qFloor(percent));
}

void WidgetTorrent::setStatus()
{
    auto torr_status = torrent->GetHandle().status().state;
    auto isPaused = torrent->GetHandle().status().paused;
    status = torr_status;
    if (isPaused) {
        ui->status->setText("paused");
    }
    else {
        switch (torr_status)
        {
        case libtorrent::torrent_status::downloading:
            ui->status->setText("downloading");
            break;
        case libtorrent::torrent_status::checking_files:
            ui->status->setText("checking files...");
            break;
        case libtorrent::torrent_status::downloading_metadata:
            ui->status->setText("downloading_metadata");
            break;
        case libtorrent::torrent_status::finished:
            ui->status->setText("finished");
            break;
        case libtorrent::torrent_status::seeding:
            ui->status->setText("seeding");
            break;
        case libtorrent::torrent_status::checking_resume_data:
            ui->status->setText("checking_resume_data");
            break;
        }
    }
}

void WidgetTorrent::setPeersSeeds()
{
    this->peers = this->torrent->GetStats().GetCountPeers();
    this->seeds = this->torrent->GetStats().GetCountSeeds();
    ui->peersSeeds->setText(QString::number(peers) + "/" + QString::number(seeds));
}

void WidgetTorrent::setUpRate()
{
    bitrate = this->torrent->GetStats().GetUploadRate();
    ui->upRate->setText(convert(bitrate) + "/s");
}

void WidgetTorrent::setDownRate()
{
    //downrate = 10000;
    this->downrate = this->torrent->GetStats().GetDownloadRate();
    ui->downRate->setText(convert(downrate) + "/s");
}

void WidgetTorrent::slotOpen()
{
    emit signalOpen(index);
}

void WidgetTorrent::slotShowInFolder()
{
    emit signalShowInFolder(index);
}

void WidgetTorrent::slotCopyMagnet()
{
    emit signalCopyMagnet(index);
}

void WidgetTorrent::slotPlace(enum Places place)
{
    emit signalPlace(index, place);
}

void WidgetTorrent::slotSharingSpeed(int speed)
{
    if (speed > 0) {
        torrent->GetHandle().set_upload_limit(speed);
    }
    else {
        torrent->GetHandle().upload_limit();
    }
    emit signalSharingSpeed(index, speed);
}

void WidgetTorrent::slotReceivingSpeed(int speed)
{
    if (speed > 0) {
        torrent->GetHandle().set_download_limit(speed);
    }
    else {
        torrent->GetHandle().download_limit();
    }
    emit signalReceivingSpeed(index, speed);
}

void WidgetTorrent::slotDelete()
{
    emit signalChangeStatus(index, deleted);
}

void WidgetTorrent::on_startStopButton_clicked()
{
    if (torrent->GetHandle().is_paused())
    {
        torrent->GetHandle().resume();
        setPauseIcon(widget_theme);
        return;
    }
    torrent->GetHandle().pause();
    setStartIcon(widget_theme);

    //emit signalChangeStatus(index, status);
}

QString WidgetTorrent::convert(unsigned long long number)
{
    if (number >= 0 && number < 1024)
    {
        return QString::number(number) + "byte";
    }
    number = number / 1024;
    if (number > 0 && number < 1024)
    {
        return QString::number(number) + "Kb";
    }
    number = number / 1024;
    if (number > 0 && number < 1024)
    {
        return QString::number(number) + "Mb";
    }
    number = number / 1024;
    if (number > 0 && number < 1024)
    {
        return QString::number(number) + "Gb";
    }
    return "";
}

std::shared_ptr<Torrent> WidgetTorrent::getTorrent() {
    return torrent;
}

void WidgetTorrent::on_settingsButton_clicked()
{
    if (dialogTorrent->isHidden())
    {
        dialogTorrent->show();
    }
    else
    {
        dialogTorrent->hide();
    }
}

