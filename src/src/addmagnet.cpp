#include "addmagnet.h"
#include "ui_addmagnet.h"

AddMagnet::AddMagnet(QDialog* parent) :
QDialog(parent),
ui(new Ui::AddMagnet)
{
    ui->setupUi(this);
}

AddMagnet::~AddMagnet()
{
    delete ui;
}

void AddMagnet::on_addTorrent_clicked()
{
    if (ui->torrentLink->text() != QString("")) {
        emit addMagnetTorrent(ui->torrentLink->text());
        emit magnetAddClosed();
        this->close();
    }
        
}

void AddMagnet::closeEvent(QCloseEvent* event) 
{
    emit magnetAddClosed();
    this->close();
}