#include "generalwindow.h"
#include "ui_generalwindow.h"
#include "gridlayoututil.h"
#include <qmessagebox.h>

GeneralWindow::GeneralWindow(std::shared_ptr<SessionHandler> ses, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GeneralWindow),
    session(ses)
{
    ui->setupUi(this);

    ui->spaceForTorrents->setLayout(new QVBoxLayout);
    QScrollArea* scroll_area = new QScrollArea;
    scroll_area->setWidgetResizable(true);
    QWidget* contentWidget = new QWidget;
    scroll_area->setWidget(contentWidget);
    QGridLayout* grid = new QGridLayout(contentWidget);
    scroll_area->setMinimumHeight(0);
    scroll_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    grid->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    grid->setHorizontalSpacing(230);
    grid->setVerticalSpacing(210);
    ui->spaceForTorrents->layout()->addWidget(scroll_area);
        
    torrentsGridLayout = grid;

    optionsWindow = new OptionsWindow;
    connect(optionsWindow, &OptionsWindow::enableReceiveLimit, this, &GeneralWindow::enableReceiveLimit);
    connect(optionsWindow, &OptionsWindow::enableShareLimit, this, &GeneralWindow::enableShareLimit);
    connect(optionsWindow, &OptionsWindow::disableReceiveLimit, this, &GeneralWindow::disableReceiveLimit);
    connect(optionsWindow, &OptionsWindow::disableShareLimit, this, &GeneralWindow::disableShareLimit);
    connect(optionsWindow, &OptionsWindow::signalSetTheme, this, &GeneralWindow::setTheme);
    connect(optionsWindow, &OptionsWindow::savePathChanged, this, &GeneralWindow::changeSavePath);
    optionsWindow->setThemeChecked(theme);
    optionsWindow->setSavePath(QString::fromStdString(session->save_path));
    

    this->setFixedSize(1024, 768); 
}

GeneralWindow::~GeneralWindow()
{
    
    delete ui;
}

void GeneralWindow::slotAddMagnetClosed() {
    addMagnetWindow = nullptr;
}

void GeneralWindow::on_buttonAddMagnetUrl_clicked() 
{
    if (addMagnetWindow == nullptr) {
        addMagnetWindow = new AddMagnet;
        connect(addMagnetWindow, &AddMagnet::addMagnetTorrent,  this, &GeneralWindow::slotAddMagnetLink);
        connect(addMagnetWindow, &AddMagnet::magnetAddClosed,  this, &GeneralWindow::slotAddMagnetClosed);
        addMagnetWindow->show();
    }
}

void GeneralWindow::enableReceiveLimit(int speed) 
{
    if (speed == 0)
        speed = 1;
    session->getLtSession()->set_download_rate_limit(speed);
}

void GeneralWindow::enableShareLimit(int speed)
{
    if (speed == 0)
        speed = 1;
    session->getLtSession()->set_upload_rate_limit(speed);
}

void GeneralWindow::disableShareLimit()
{
    session->getLtSession()->set_download_rate_limit(0);
}

void GeneralWindow::disableReceiveLimit()
{
    session->getLtSession()->set_download_rate_limit(0);
}

void GeneralWindow::changeSavePath(QString path) {
    session->save_path = path.toStdString();
}

void GeneralWindow::slotAddMagnetLink(QString link) 
{
    addTorrent(link, torrentType::byLink);
}

void GeneralWindow::on_addTorrentButton_clicked()
{
    QString path = QFileDialog::getOpenFileName(0, "Choose torrent to download", "", "*.torrent");
    if (path == "")
        return;
    addTorrent(path);
}

void GeneralWindow::slotSize(size_t index, unsigned long long size)
{
    try
    {
        widgetTorrents.at(index).first->setSize(size);
    }
    catch(std::out_of_range const& e)
    {
        std::cout << "Incorrect index" << std::endl;
    }
}

void GeneralWindow::on_optionsButton_clicked()
{
    if (optionsWindow->isHidden()) {
        optionsWindow->show();
    }
    else {
        optionsWindow->hide();
    }
}

void GeneralWindow::reAddTorrents() {
    auto cnt = 0;
    for (auto widg : widgetTorrents) {
        
        auto column = (cnt) % 4;
        adding_index = cnt / 4;

        widg.first->row_column = { adding_index, column };
        torrentsGridLayout->addWidget(widg.first, adding_index, column);
        for (auto i : widgetTorrents) {
            i.first->resize(300, 300);
        }
        cnt++;
    }
}


void GeneralWindow::addTorrent(QString path, torrentType type, bool inProgram) {
    Torrent torr;
    try {
        if (type == torrentType::byPath) {

            lt::add_torrent_params add_torrent_params;
            add_torrent_params.ti = std::make_shared<lt::torrent_info>(path.toStdString());



            auto name = add_torrent_params.name;
            auto sameElementIter = std::find_if(widgetTorrents.begin(), widgetTorrents.end(), [name](std::pair<WidgetTorrent*, int> x) { return x.first->getTorrent()->GetHandle().name() == name; });
            if (sameElementIter != widgetTorrents.end()) {
                QMessageBox messageBox;
                messageBox.warning(0, "Error", "This torrent already added!");
                messageBox.setFixedSize(500, 200);
                messageBox.show();
                return;
            }


            add_torrent_params.save_path = session->save_path;
            torr = session->getLtSession()->add_torrent(add_torrent_params);
            torr.type = torrentType::byPath;



            }
    }
    catch (...) {
        if (inProgram) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Invalid torrent file!");
            messageBox.setFixedSize(500, 200);
            messageBox.show();
        }
        return;
    }
    try {
        if (type == torrentType::byLink) {


            lt::add_torrent_params add_torrent_params = lt::parse_magnet_uri(path.toStdString());

            auto name = add_torrent_params.name;
            auto sameElementIter = std::find_if(widgetTorrents.begin(), widgetTorrents.end(), [name](std::pair<WidgetTorrent*, int> x) { return x.first->getTorrent()->GetHandle().name() == name; });
            if (sameElementIter != widgetTorrents.end()) {
                QMessageBox messageBox;
                messageBox.warning(0, "Error", "This torrent already added!");
                messageBox.setFixedSize(500, 200);
                messageBox.show();
                return;
            }

            add_torrent_params.save_path = session->save_path;
            torr = session->getLtSession()->add_torrent(add_torrent_params);
            torr.type = torrentType::byLink;


        }
    }
    catch (...) {
        if (inProgram) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Invalid torrent link!");
            messageBox.setFixedSize(500, 200);
            messageBox.show();
        }
        return;
    }
    auto torrent = std::make_shared<Torrent>(torr);
    QTimer* timer = new QTimer;
    timer->start(50);
    lastAddedNumber++;//-
    torrentIdx++;
    WidgetTorrent* tmp = new WidgetTorrent(lastAddedNumber, torrent, path);//-
    connect(tmp, &WidgetTorrent::signalChangeStatus, this, &GeneralWindow::slotChangeStatus);//-
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::setDownRate);
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::setUpRate);
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::setPeersSeeds);
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::setDownRate);
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::setProgress);
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::setStatus);
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::resizes);
    connect(timer, &QTimer::timeout, tmp, &WidgetTorrent::setStartPauseIcon);
    widgetTorrents.push_back(std::make_pair(tmp, lastAddedNumber));//-
    emit signalAddedTorrent(static_cast<size_t>(lastAddedNumber));//-
    tmp->setTorrentName(path);//~
    
    auto column = (widgetTorrents.size() - 1)  %  4;
    adding_index = (widgetTorrents.size() -1) / 4;
        
    tmp->row_column = { adding_index, column };
    torrentsGridLayout->addWidget(tmp, adding_index, column);
    for (auto i : widgetTorrents) {
        i.first->resize(300, 300);
    }

    //int countColumn = ui->spaceForTorrents->width() / WIDGET_WIDTH + 1;
    //int i = (widgetTorrents.size() - 1) / countColumn;
    //int j = (widgetTorrents.size() - 1) - i * countColumn;

//    ui->spaceForTorrents->setRowCount(i + 1);
 //   ui->spaceForTorrents->setColumnCount(countColumn);
//    ui->spaceForTorrents->setCellWidget(i, j, tmp);//~
//    ui->spaceForTorrents->resizeColumnsToContents();
 //   ui->spaceForTorrents->resizeRowsToContents();

    tmp->setTheme(theme);
}


/*
void GeneralWindow::slotProgress(size_t index, size_t percent)
{
    try
    {
        widgetTorrents.at(index).first->setProgress(percent);
    }
    catch(std::out_of_range const& e)
    {
        std::cout << "Incorrect index" << std::endl;
    }
}

void GeneralWindow::slotStatus(size_t index, TorrentStatus status)
{
    try
    {
        widgetTorrents.at(index).first->setStatus(status);
    }
    catch(std::out_of_range const& e)
    {
        std::cout << "Incorrect index" << std::endl;
    }
}

void GeneralWindow::slotPeersSeeds(size_t index, size_t peers, size_t seeds)
{
    try
    {
        widgetTorrents.at(index).first->setPeersSeeds(peers, seeds);
    }
    catch(std::out_of_range const& e)
    {
        std::cout << "Incorrect index" << std::endl;
    }
}

void GeneralWindow::slotUpRate(size_t index, unsigned long long bitrate)
{
    try
    {
        widgetTorrents.at(index).first->setUpRate(bitrate);
    }
    catch(std::out_of_range const& e)
    {
        std::cout << "Incorrect index" << std::endl;
    }
}

void GeneralWindow::slotDownRate(size_t index, unsigned long long downrate)
{
    try
    {
        widgetTorrents.at(index).first->setDownRate(downrate);
    }
    catch(std::out_of_range const& e)
    {
        std::cout << "Incorrect index" << std::endl;
    }
}
*/
void GeneralWindow::on_nightModeButton_clicked()
{
    if (theme == themes::light) {
        setTheme(themes::dark);
        optionsWindow->setThemeChecked(themes::dark);
    }
    else {
        setTheme(themes::light);
        optionsWindow->setThemeChecked(themes::light);
    }
}

void GeneralWindow::on_buttonDeleteAll_clicked()
{
    for (int i = widgetTorrents.size() - 1; i >= 0;i--) {
        auto row = widgetTorrents[i].first->row_column.first;
        auto column = widgetTorrents[i].first->row_column.second;
        try {
            if (session && session->getLtSession() && widgetTorrents[i].first && widgetTorrents[i].first->getTorrent())
                session->getLtSession()->remove_torrent(widgetTorrents[i].first->getTorrent()->GetHandle());
        }
        catch (...) {
            try {
                widgetTorrents[i].first->getTorrent()->GetHandle().pause();
            }
            catch (...) {

            }
        }
        GridLayoutUtil::removeCell(torrentsGridLayout, row, column);
        widgetTorrents.pop_back();
    }
}



void GeneralWindow::slotChangeStatus(size_t index, TorrentStatus status)
{
    qDebug() << "I called";
    emit signalChangeStatus(index, status);
    if (status == deleted)
    {

        auto removeElementIter = std::find_if(widgetTorrents.begin(), widgetTorrents.end(), [index](std::pair<WidgetTorrent*, int> x) { return x.second == index; });
        
        if (removeElementIter != widgetTorrents.end()) {
            auto row = removeElementIter->first->row_column.first;
            auto column = removeElementIter->first->row_column.second;
            try {
                int cnt = 0;
                auto name = removeElementIter->first->getTorrent()->GetHandle().name();

                for (auto i : widgetTorrents){
                    if (i.first->getTorrent()->GetHandle().name() == name)
                        cnt++;
                }
                if (cnt==1) {
                    if (session && session->getLtSession() && removeElementIter->first && removeElementIter->first->getTorrent())
                        session->getLtSession()->remove_torrent(removeElementIter->first->getTorrent()->GetHandle());
                }
            }
            catch (...) {
                try {
                    removeElementIter->first->getTorrent()->GetHandle().pause();
                }
                catch (...) {

                }
            }
            GridLayoutUtil::removeCell(torrentsGridLayout, row, column);
            widgetTorrents.erase(removeElementIter);
        }
        try {
            reAddTorrents();
        }
        catch(...){}
    }
}

void GeneralWindow::slotOpen(size_t index)
{
    emit signalOpen(index);
}

void GeneralWindow::slotShowInFolder(size_t index)
{
    emit signalShowInFolder(index);
}

void GeneralWindow::slotCopyMagnet(size_t index)
{
    emit signalCopyMagnet(index);
}

void GeneralWindow::slotPlace(size_t index, enum Places place)
{
    emit signalPlace(index, place);
    //дописать код
}

void GeneralWindow::slotSharingSpeed(size_t index, int speed)
{
    emit signalSharingSpeed(index, speed);
}

void GeneralWindow::slotReceivingSpeed(size_t index, int speed)
{
    emit signalReceivingSpeed(index, speed);
}



void GeneralWindow::setTheme(themes new_theme)
{
   
    switch (new_theme) {
    case themes::light:
        //ui->addTorrentButton->setIcon(QIcon(":/light/image/light/light-plus-icon.ico"));
        ui->addTorrentButton->setIcon(QIcon(":/light/image/light/light-plus-icon.ico"));
        ui->buttonAddMagnetUrl->setIcon(QIcon(":/light/image/light/light-link-icon.ico"));
        ui->buttonDeleteAll->setIcon(QIcon(":/light/image/light/light-trash-icon.ico"));
        ui->nightModeButton->setIcon(QIcon(":/light/image/light/light-moon-icon.ico"));
        ui->optionsButton->setIcon(QIcon(":/light/image/light/light-settings-icon.ico"));
        ui->buttonsFrame->setStyleSheet("background-color: rgb(141, 141, 141);");
        ui->spaceForTorrents->setStyleSheet("background-color: rgb(255, 255, 255);");
        break;
    case themes::dark:
        ui->addTorrentButton->setIcon(QIcon(":/dark/image/dark/dark-plus-icon.ico"));
        ui->buttonAddMagnetUrl->setIcon(QIcon(":/dark/image/dark/dark-link-icon.ico"));
        ui->buttonDeleteAll->setIcon(QIcon(":/dark/image/dark/dark-trash-icon.ico"));
        ui->nightModeButton->setIcon(QIcon(":/dark/image/dark/dark-sun-icon.ico"));
        ui->optionsButton->setIcon(QIcon(":/dark/image/dark/dark-settings-icon.ico"));
        ui->buttonsFrame->setStyleSheet("background-color: rgb(40, 40, 40);");
        ui->spaceForTorrents->setStyleSheet("background-color: rgb(64, 64, 64);");
        break;
    case themes::green:
        ui->addTorrentButton->setIcon(QIcon(":/green/image/green/green-plus-icon.ico"));
        ui->buttonAddMagnetUrl->setIcon(QIcon(":/green/image/green/green-link-icon.ico"));
        ui->buttonDeleteAll->setIcon(QIcon(":/green/image/green/green-trash-icon.ico"));
        ui->nightModeButton->setIcon(QIcon(":/green/image/green/green-sun-icon.ico"));
        ui->optionsButton->setIcon(QIcon(":/green/image/green/green-settings-icon.ico"));
        ui->buttonsFrame->setStyleSheet("background-color: rgb(0, 61, 22);");
        ui->spaceForTorrents->setStyleSheet("background-color: rgb(145, 252, 141);");
        break;
    case themes::blue:
        ui->addTorrentButton->setIcon(QIcon(":/blue/image/blue/blue-plus-icon.ico"));
        ui->buttonAddMagnetUrl->setIcon(QIcon(":/blue/image/blue/blue-link-icon.ico"));
        ui->buttonDeleteAll->setIcon(QIcon(":/blue/image/blue/blue-trash-icon.ico"));
        ui->nightModeButton->setIcon(QIcon(":/blue/image/blue/blue-sun-icon.ico"));
        ui->optionsButton->setIcon(QIcon(":/blue/image/blue/blue-settings-icon.ico"));
        ui->buttonsFrame->setStyleSheet("background-color: rgb(0, 0, 120);");
        ui->spaceForTorrents->setStyleSheet("background-color: rgb(0, 32, 242);");
        break;
    case themes::red:
        ui->addTorrentButton->setIcon(QIcon(":/red/image/red/red-plus-icon.ico"));
        ui->buttonAddMagnetUrl->setIcon(QIcon(":/red/image/red/red-link-icon.ico"));
        ui->buttonDeleteAll->setIcon(QIcon(":/red/image/red/red-trash-icon.ico"));
        ui->nightModeButton->setIcon(QIcon(":/red/image/red/red-sun-icon.ico"));
        ui->optionsButton->setIcon(QIcon(":/red/image/red/red-settings-icon.ico"));
        ui->buttonsFrame->setStyleSheet("background-color: rgb(171, 0, 12);");
        ui->spaceForTorrents->setStyleSheet("background-color: rgb(250, 79, 91);");
        break;
    }
    for (auto i : widgetTorrents) {
        i.first->setTheme(new_theme);
    }
    theme = new_theme;

}