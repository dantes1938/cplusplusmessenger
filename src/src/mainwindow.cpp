#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include "mainwindow.ui"
#include "torrent.h"
#include "torrent_info.h"
#include <QTimer>
#include "Save.h"
#include <QObject>
#include <qformlayout.h>
#include <QComboBox>
#include <qstandardpaths.h>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //widgetTorrent = new WidgetTorrent();
    //widgetTorrent->show();
//    widgetTorrent->slotTorrentNum(1);
//    connect(this, &MainWindow::signalTorrentNum, widgetTorrent, &WidgetTorrent::slotTorrentNum);
//    connect(this, &MainWindow::signalProgress, widgetTorrent, &WidgetTorrent::slotProgress);
    lt::session_params old_session =  loadSessionParams();
    std::shared_ptr<SessionHandler> ses = std::make_shared<SessionHandler>(SessionHandler(old_session));
    qDebug() << ses->getLtSession()->get_torrents().size();
    ses->save_path =  QStandardPaths::writableLocation(QStandardPaths::DownloadLocation).toStdString();
    generalWindow = new GeneralWindow(ses);
    loadTorrents(generalWindow);
    generalWindow->show();


    




    










    
    //generalWindow->setUpRate(1, 100);
    //QTimer* timer = new QTimer();
    //std::string path_to_torrent = "C:\\UbuTorrent\\crowns-and-pawns-kingdom-of-deceit.torrent";
    //std::string save_path;

    //lt::add_torrent_params atp;
    //atp.ti = std::make_shared<lt::torrent_info>(path_to_torrent);
    //lt::torrent_handle h = ses.add_torrent(atp);
    //Info inf(h);
    //save("C:\\UbuTorrent\\save.txt", ses);
    //load("C:\\UbuTorrent\\save.txt");

    //on_addTorrentButton_clicked()
    //
    //QObject::connect(timer, SIGNAL(timeout()), generalWindow, SLOT(slotUpRate((size_t)1, (long long)inf.GetUploadRate())));
    //timer->start(100);
    
//    connect(this, &MainWindow::signalStatus, widgetTorrent, &WidgetTorrent::slotStatus);
//    connect(this, &MainWindow::signalPeersSeeds, widgetTorrent, &WidgetTorrent::slotPeersSeeds);
//    connect(this, &MainWindow::signalUpRate, widgetTorrent, &WidgetTorrent::slotUpRate);
//    connect(this, &MainWindow::signalDownRate, widgetTorrent, &WidgetTorrent::slotDownRate);
    //connect(this, &MainWindow::signalConvert, widgetTorrent, &WidgetTorrent::slotConvert);


}

MainWindow::~MainWindow()
{
    saveTorrents(generalWindow);
    generalWindow->disableShareLimit();
    generalWindow->disableReceiveLimit();
    save(generalWindow->session->getLtSession());
    delete ui;
}

void MainWindow::saveTorrents(GeneralWindow* generalWindow) {
    std::ofstream write;
    write.open(".torrent");
    for (auto i : generalWindow->widgetTorrents) {
        std::string prefix = "";
        if (i.first->getTorrent()->type == torrentType::byLink) {
            prefix = "magnett:";
        }
            
        if (i.first->getTorrent()->type == torrentType::byPath) {
            prefix = "torrent:";
        }
            
        write << (prefix + i.first->torrentFilePath.toStdString()) << std::endl;
    }
}

void MainWindow::loadTorrents(GeneralWindow* generalWindow) {
    std::ifstream read;
    read.open(".torrent");
    std::string line;
    while (getline(read, line))
    {
        if (line.size() > 10) {
            std::string prefix = line.substr(0, 8);
            std::string info = line.substr(8, line.size() - 8);
            torrentType type;
            if (prefix == "magnett:") {
                type = torrentType::byLink;
            }
            else if (prefix == "torrent:") {
                type = torrentType::byPath;
            }
            else {
                continue;
            }
            QString path = QString::fromStdString(info);
            if (type == torrentType::byPath) {
                if (QFile::exists(path))
                    generalWindow->addTorrent(path, type, false);
            }
            else {
                generalWindow->addTorrent(path, type, false);
            }
        }
    }
}

void MainWindow::on_pushButton_clicked()
{
//    emit signalTorrentNum(rand() % 10);
//    count += 2;
//    emit signalProgress(count);
//    if(count < 100)
//    {
//        status = "downloading";
//    }
//    else
//    {
//        status = "done!";
//    }
//    emit signalStatus(status);
//    peers = rand() % 100;
//    seeds = rand() % 100;
//    emit signalPeersSeeds(peers, seeds);
//    UpRate = rand() % 1000;
//    emit signalUpRate(UpRate);
//    DownRate = rand() % 1000;
//    emit signalDownRate(DownRate);
    /*emit signalConvert(12);
    emit signalConvert(1042);
    emit signalConvert(2105);
    emit signalConvert(5011);*/
}

