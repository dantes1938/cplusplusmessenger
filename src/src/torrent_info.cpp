#include "torrent_info.h"

Info::Info(lt::torrent_handle th) : torrent_handle(th) {
	Update();
}

void Info::Update() {
	lt::torrent_status s = torrent_handle.status();
	count_peers = s.num_peers;
	count_seeds = s.num_seeds;
	progress = s.progress;
	upload_rate = s.upload_rate;
	download_rate = s.download_rate;
	is_seeding = s.is_seeding;
	error = s.errc;
	state = s.state;
	total_done = s.total_done;
	total = s.total;
	name = s.name;
}

int Info::GetCountPeers() {
	return count_peers;
}

int Info::GetCountSeeds() {
	return count_seeds;
}

float Info::GetProgress() {
	return progress;
}

int Info::GetUploadRate() {
	return upload_rate;
}

int Info::GetDownloadRate() {
	return download_rate;
}

bool Info::IsSeeding() {
	return is_seeding;
}

bool Info::Error() {
	return (bool)error;
}

std::string Info::GetErrorMessage() {
	return error.what();
}

int Info::GetErrorCode() {
	return error.value();
}

std::int64_t Info::GetTotal() {
	return total;
}

std::int64_t Info::GetTotalDone() {
	return total_done;
}

std::string Info::GetName() {
	return name;
}

std::string Info::GetState() {
	switch (state) {
	case lt::torrent_status::checking_files: return "checking";
	case lt::torrent_status::downloading_metadata: return "downloading metadata";
	case lt::torrent_status::downloading: return "downloading";
	case lt::torrent_status::finished: return "finished";
	case lt::torrent_status::seeding: return "seeding";
	case lt::torrent_status::checking_resume_data: return "checking resume";
	default: return "<>";
	}
}
