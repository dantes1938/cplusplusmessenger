#include"Save.h"
#include <QDebug>
#include <libtorrent/session_params.hpp>
using namespace std;
void save(std::shared_ptr<lt::session> ses)
{
    std::ofstream of(".session", std::ios_base::binary);
    of.unsetf(std::ios_base::skipws);
    auto const b = write_session_params_buf(ses->session_state()
        , lt::save_state_flags_t::all());
    of.write(b.data(), int(b.size()));
    qDebug() << "try save";
}

std::vector<char> load_file(char const* filename)
{
    std::ifstream ifs(filename, std::ios_base::binary);
    ifs.unsetf(std::ios_base::skipws);
    return { std::istream_iterator<char>(ifs), std::istream_iterator<char>() };
}

lt::session_params loadSessionParams() {
    auto session_params = load_file(".session");
    lt::session_params params = session_params.empty()
        ? lt::session_params() : lt::read_session_params(session_params);
    params.settings.set_int(lt::settings_pack::alert_mask
        , lt::alert_category::error
        | lt::alert_category::storage
        | lt::alert_category::status);

    if (!session_params.empty()) {
        qDebug() << "loaded";
    }
    else {
        qDebug() << "not loaded";
    }
        

    return params;
}