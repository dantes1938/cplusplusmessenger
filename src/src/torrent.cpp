#include "torrent.h"
#include <chrono>


Torrent::Torrent(lt::torrent_handle th) : torrent_handle(th) {}

Torrent::Torrent() : torrent_handle() {}

Info Torrent::GetStats() {
	return { torrent_handle };
}

lt::torrent_handle Torrent::GetHandle() {
	return torrent_handle;
};

void TorrentList::addTorrent(lt::torrent_handle torrent_handle) {
	torrents.push_back(std::make_shared<Torrent>(Torrent(torrent_handle)));
}

SessionHandler::SessionHandler(lt::session_params old_session) : session(std::make_shared<lt::session>(lt::session(old_session))) {}

Torrent SessionHandler::AddTorrentByPath(const std::string& path_to_torrent) {
	lt::add_torrent_params add_torrent_params;
	add_torrent_params.ti = std::make_shared<lt::torrent_info>(path_to_torrent);
	add_torrent_params.save_path = save_path;
	Torrent torrent_ = session->add_torrent(add_torrent_params);
	torrent_.type = torrentType::byPath;
	return { torrent_ };
}

Torrent SessionHandler::AddTorrentByLink(const std::string& link) {
	lt::add_torrent_params add_torrent_params = lt::parse_magnet_uri(link);
	lt::torrent_info ti(add_torrent_params.info_hashes);
	std::cout << "torrent " << ti.name() << ": " << ti.total_size() / 1024 << " Kb\n";
	add_torrent_params.save_path = save_path;
	Torrent torrent_ = session->add_torrent(add_torrent_params);
	torrent_.type = torrentType::byLink;
	return { torrent_ };
}

void SessionHandler::PopAlerts(std::vector<lt::alert*>* alerts) {
	session->pop_alerts(alerts);
}

void SessionHandler::PostTorrentUpdates() {
	session->post_torrent_updates();
}

std::shared_ptr<lt::session> SessionHandler::getLtSession() {
	return session;
}
