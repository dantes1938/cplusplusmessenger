#include "dialogtorrent.h"
#include "ui_dialogtorrent.h"

DialogTorrent::DialogTorrent(std::shared_ptr<Torrent> torrent_, QWidget *parent)  :
    QWidget(parent),
    ui(new Ui::DialogTorrent),
    torrent(torrent_)
{
    ui->setupUi(this);
}

DialogTorrent::~DialogTorrent()
{
    delete ui;
}


void DialogTorrent::on_openButton_clicked()
{
    QString path = QString("file:///") + QString::fromStdString(torrent->GetHandle().status().save_path) + '\\' + QString::fromStdString(torrent->GetHandle().status().name);
    QDesktopServices::openUrl(QUrl(path));
    emit signalOpen();
}

void DialogTorrent::on_showInFolderButton_clicked()
{
    QString path = QString("file:///") + QString::fromStdString(torrent->GetHandle().status().save_path);
    QDesktopServices::openUrl(QUrl(path));
    emit signalShowInFolder();
}

void DialogTorrent::on_copyMagnetButton_clicked()
{
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(QString::fromStdString(lt::make_magnet_uri(torrent->GetHandle())));
    emit signalCopyMagnet();
}

void DialogTorrent::on_changePlaceBox_currentIndexChanged(int index)
{
    Places place = toUp;
    switch (index)
    {
        case 0:
            place = toUp;
            break;
        case 1:
            place = toDown;
            break;
        case 2:
            place = toBegin;
            break;
        case 3:
            place = toEnd;
            break;
    }
    emit signalPlace(place);
}

void DialogTorrent::on_sharingSpeedBox_valueChanged(int arg1)
{
    int tmp = 0;
    switch(ui->comboBoxShare->currentIndex())
    {
        case 0:
            tmp = 1024;
            break;
        case 1:
            tmp = 1024*1024;
            break;
        case 2:
            tmp = 1024*1024*1024;
            break;
    }
    emit signalSharingSpeed(arg1*tmp);
}

void DialogTorrent::on_comboBoxShare_currentIndexChanged(int index)
{
    int tmp = 0;
    switch (index)
    {
        case 0:
            tmp = 1024;
            break;
        case 1:
            tmp = 1024*1024;
            break;
        case 2:
            tmp = 1024*1024*1024;
            break;
    }
    emit signalSharingSpeed(tmp*ui->sharingSpeedBox->value());
}


void DialogTorrent::on_receivingSpeedBox_valueChanged(int arg1)
{
    int tmp = 0;
    switch(ui->comboBoxReceive->currentIndex())
    {
        case 0:
            tmp = 1024;
            break;
        case 1:
            tmp = 1024*1024;
            break;
        case 2:
            tmp = 1024*1024*1024;
            break;
    }
    emit signalReceivingSpeed(arg1*tmp);
}

void DialogTorrent::on_comboBoxReceive_currentIndexChanged(int index)
{
    int tmp = 0;
    switch (index)
    {
        case 0:
            tmp = 1024;
            break;
        case 1:
            tmp = 1024*1024;
            break;
        case 2:
            tmp = 1024*1024*1024;
            break;
    }
    emit signalReceivingSpeed(tmp*ui->receivingSpeedBox->value());
}

void DialogTorrent::on_deleteButton_clicked()
{
    emit signalDelete();
    this->close();
}
