#include "optionswindow.h"
#include "ui_optionswindow.h"
#include <QTabWidget>
#include <QTabBar>
#include <QFileDialog>
#include <QMessageBox>

OptionsWindow::OptionsWindow(QMainWindow* parent)
    : QMainWindow(parent)
    , ui(new Ui::OptionsWindow)
{
    ui->setupUi(this);
    this->on_interfaceButton_clicked();
    ui->speedBox1->setCheckable(true);
    ui->speedBox2->setCheckable(true);
    ui->speedBox1->setChecked(false);
    ui->speedBox2->setChecked(false);

    connect(ui->OkSpeedButton, SIGNAL(clicked()), this, SLOT(OkButton_clicked()));
    connect(ui->OkFoldersButton, SIGNAL(clicked()), this, SLOT(OkButton_clicked()));
    connect(ui->OkInterButton, SIGNAL(clicked()), this, SLOT(OkButton_clicked()));
    connect(ui->OkInfoButton, SIGNAL(clicked()), this, SLOT(OkButton_clicked()));



    this->setWindowTitle("Options");
    //this->SetColor("#404040", "#606060", "#fff");
    this->on_interfaceButton_clicked();
    //this->on_darkBox_clicked();
    this->setFixedSize(650, 460);
}

void OptionsWindow::on_interfaceButton_clicked()
{
    ui->interfaceButton->setStyleSheet("background: " + hover + "; font-weight: bold;");
    ui->foldersButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->speedButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->infoButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->menuTab->setCurrentIndex(1);
}

void OptionsWindow::on_pathButton_clicked()
{
    QString path = QFileDialog::getExistingDirectory(0, "Choose save directory");
    if (path == QString(""))
        return;
    ui->folderDownloadPath->setText(path);
}

void OptionsWindow::on_changeButton_clicked() {
    if (QDir(ui->folderDownloadPath->text()).exists()) {
        emit savePathChanged(ui->folderDownloadPath->text());
    }
    else {
        QMessageBox messageBox;
        messageBox.warning(0, "Error", "Folder doesn't exist!");
        messageBox.setFixedSize(500, 200);
        messageBox.show();
    }
}

void OptionsWindow::on_sharingSpeedBox_valueChanged(int arg1){
    int speed = 0;
    if (ui->comboBoxShare->currentIndex() == 0) {
        speed = arg1 * 1024;
    }
    else if(ui->comboBoxShare->currentIndex() == 1) {
        speed = arg1 * 1024 * 1024;
    }
    else {
        speed = arg1 * 1024 * 1024 * 1024;
    }
    
    emit enableShareLimit(speed);
}

void OptionsWindow::on_receivingSpeedBox_valueChanged(int arg1) {
    int speed = 0;
    if (ui->comboBoxReceive->currentIndex() == 0) {
        speed = arg1 * 1024;
    }
    else if(ui->comboBoxReceive->currentIndex() == 1) {
        speed = arg1 * 1024 * 1024;
    }
    else {
        speed = arg1 * 1024 * 1024 * 1024;
    
    }
    emit enableReceiveLimit(speed);
}

void OptionsWindow::on_speedBox1_toggled(bool state)
{
    if (!state) {
        emit disableShareLimit();
        return;
    }
    int speed = 0;
    if (ui->comboBoxShare->currentIndex() == 0) {
        speed = ui->sharingSpeedBox->value() * 1024;
    }
    else if(ui->comboBoxShare->currentIndex() == 1) {
        speed = ui->sharingSpeedBox->value() * 1024 * 1024;
    }
    else {
        speed = ui->sharingSpeedBox->value() * 1024 * 1024 * 1024;
    }

    emit enableShareLimit(speed);

}

void OptionsWindow::on_speedBox2_toggled(bool state)
{
    if (!state) {
        emit disableReceiveLimit();
        return;
    }
    int speed = 0;
    if (ui->comboBoxShare->currentIndex() == 0) {
        speed = ui->receivingSpeedBox->value() * 1024;
    }
    else if(ui->comboBoxShare->currentIndex() == 1) {
        speed = ui->receivingSpeedBox->value() * 1024 * 1024;
    }
    else {
        speed = ui->receivingSpeedBox->value() * 1024 * 1024 * 1024;
    }

    emit enableReceiveLimit(speed);

}


void OptionsWindow::setThemeChecked(themes theme)
{
    switch(theme)
    {
    case themes::light:
        this->on_lightBox_clicked();
        break;
    case themes::dark:
        this->on_darkBox_clicked();
        break;
    case themes::red:
        this->on_redBox_clicked();
        break;
    case themes::green:
        this->on_greenBox_clicked();
        break;
    case themes::blue:
        this->on_blueBox_clicked();
        break;
    }
}

OptionsWindow::~OptionsWindow()
{
    delete ui;
}



void OptionsWindow::on_foldersButton_clicked()
{
    ui->interfaceButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->foldersButton->setStyleSheet("background: " + hover + "; font-weight: bold;");
    ui->speedButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->infoButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->menuTab->setCurrentIndex(0);
}

void OptionsWindow::on_speedButton_clicked()
{
    ui->interfaceButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->foldersButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->speedButton->setStyleSheet("background: " + hover + "; font-weight: bold;");
    ui->infoButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->menuTab->setCurrentIndex(2);
}

void OptionsWindow::on_infoButton_clicked()
{
    ui->interfaceButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->foldersButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->speedButton->setStyleSheet("QPushButton { background: " + background + "; } QPushButton::hover { background: " + hover + "; }");
    ui->infoButton->setStyleSheet("background: " + hover + "; font-weight: bold;");
    ui->menuTab->setCurrentIndex(3);
}

void OptionsWindow::setLightChecked() {
    ui->lightBox->setChecked(true);
    ui->lightBox->setEnabled(false);
    ui->darkBox->setEnabled(true);
    ui->greenBox->setEnabled(true);
    ui->redBox->setEnabled(true);
    ui->blueBox->setEnabled(true);
    ui->darkBox->setChecked(false);
    ui->blueBox->setChecked(false);
    ui->greenBox->setChecked(false);
    ui->redBox->setChecked(false);
    this->setThemes(themes::light);
    ui->lightBox->setStyleSheet("QCheckBox { background: " + hover + "; }");
    ui->darkBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->blueBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->greenBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->redBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
}

void OptionsWindow::on_lightBox_clicked()
{

    setLightChecked();
    emit signalSetTheme(themes::light);
}

void OptionsWindow::setDarkChecked() {
    ui->darkBox->setChecked(true);
    ui->darkBox->setEnabled(false);
    ui->lightBox->setEnabled(true);
    ui->greenBox->setEnabled(true);
    ui->redBox->setEnabled(true);
    ui->blueBox->setEnabled(true);
    ui->lightBox->setChecked(false);
    ui->blueBox->setChecked(false);
    ui->greenBox->setChecked(false);
    ui->redBox->setChecked(false);
    this->setThemes(themes::dark);
    ui->lightBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->darkBox->setStyleSheet("QCheckBox { background: " + hover + "; }");
    ui->blueBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->greenBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->redBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
}

void OptionsWindow::on_darkBox_clicked()
{
    setDarkChecked();
    emit signalSetTheme(themes::dark);
}

void OptionsWindow::setSavePath(QString path) {
    ui->folderDownloadPath->setText(path);

}



void OptionsWindow::setBlueChecked() {
    ui->blueBox->setChecked(true);
    ui->blueBox->setEnabled(false);
    ui->lightBox->setEnabled(true);
    ui->greenBox->setEnabled(true);
    ui->redBox->setEnabled(true);
    ui->darkBox->setEnabled(true);
    ui->lightBox->setChecked(false);
    ui->greenBox->setChecked(false);
    ui->redBox->setChecked(false);
    ui->darkBox->setChecked(false);
    this->setThemes(themes::blue);
    ui->lightBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->darkBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->blueBox->setStyleSheet("QCheckBox { background: " + hover + "; }");
    ui->greenBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->redBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
}

void OptionsWindow::on_blueBox_clicked()
{
    setBlueChecked();
    emit signalSetTheme(themes::blue);
}

void OptionsWindow::SetColor(QString background, QString hover, QString text_indicator)
{
    this->background = background;
    this->hover = hover;
    this->text_indicator = text_indicator;

    qApp->setStyleSheet("QFrame#Menu{ background: " + background + "; } #Menu QPushButton{ background: " + background + "; color: " + text_indicator + "; } #Menu QPushButton::hover { background: " + hover + "; } QCheckBox { background: " + background + "; color: " + text_indicator + "; } QCheckBox::hover { background: " + hover + ";} QCheckBox::indicator { background: " + text_indicator + "; } QCheckBox::indicator:checked { background: " + background + "; border: 3px solid " + text_indicator + "; }");
}


void OptionsWindow::setGreenChecked() {
        ui->greenBox->setChecked(true);
        ui->greenBox->setEnabled(false);
        ui->lightBox->setEnabled(true);
        ui->blueBox->setEnabled(true);
        ui->redBox->setEnabled(true);
        ui->darkBox->setEnabled(true);
        ui->lightBox->setChecked(false);
        ui->blueBox->setChecked(false);
        ui->redBox->setChecked(false);
        ui->darkBox->setChecked(false);
        this->setThemes(themes::green);
        ui->lightBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
        ui->darkBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
        ui->blueBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
        ui->greenBox->setStyleSheet("QCheckBox { background: " + hover + "; }");
        ui->redBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
}

void OptionsWindow::on_greenBox_clicked()
{
    setGreenChecked();
    emit signalSetTheme(themes::green);
}

void OptionsWindow::setRedChecked() {
    ui->redBox->setChecked(true);
    ui->redBox->setEnabled(false);
    ui->lightBox->setEnabled(true);
    ui->greenBox->setEnabled(true);
    ui->darkBox->setEnabled(true);
    ui->blueBox->setEnabled(true);
    ui->lightBox->setChecked(false);
    ui->blueBox->setChecked(false);
    ui->greenBox->setChecked(false);
    ui->darkBox->setChecked(false);
    this->setThemes(themes::red);
    ui->lightBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->darkBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->blueBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->greenBox->setStyleSheet("QCheckBox { background: " + background + "; } QCheckBox::hover { background: " + hover + "; }");
    ui->redBox->setStyleSheet("QCheckBox { background: " + hover + "; }");
}

void OptionsWindow::setThemes(themes theme) {

    if (theme == themes::light)
    {
        /*QPalette whitePalette;
        whitePalette.setColor(QPalette::Window, QColor(139, 139, 139));
        whitePalette.setColor(QPalette::WindowText, Qt::black);
        whitePalette.setColor(QPalette::Base, Qt::gray);
        whitePalette.setColor(QPalette::AlternateBase, Qt::black);
        whitePalette.setColor(QPalette::ToolTipBase, Qt::gray);
        whitePalette.setColor(QPalette::ToolTipText, Qt::black);
        whitePalette.setColor(QPalette::Text, Qt::black);
        whitePalette.setColor(QPalette::Button, QColor(139, 139, 139));
        whitePalette.setColor(QPalette::ButtonText, Qt::black);
        whitePalette.setColor(QPalette::BrightText, Qt::black);
        whitePalette.setColor(QPalette::Link, QColor(42, 130, 218));
        whitePalette.setColor(QPalette::Highlight, Qt::gray);
        whitePalette.setColor(QPalette::HighlightedText, Qt::black);
        qApp->setPalette(whitePalette);
        this->SetColor("gray", "#000000", "#000000");
        //this->SetProgressBarColor("#e7e7e7", "#9ace6a");
        this->on_interfaceButton_clicked();*/
        QPalette whitePalette;
        whitePalette.setColor(QPalette::Window, QColor(139, 139, 139));
        whitePalette.setColor(QPalette::WindowText, Qt::black);
        whitePalette.setColor(QPalette::Base, Qt::white);
        whitePalette.setColor(QPalette::AlternateBase, Qt::black);
        whitePalette.setColor(QPalette::ToolTipBase, Qt::white);
        whitePalette.setColor(QPalette::ToolTipText, Qt::white);
        whitePalette.setColor(QPalette::Text, QColor("#404040"));
        whitePalette.setColor(QPalette::Button, QColor(139, 139, 139));
        whitePalette.setColor(QPalette::ButtonText, Qt::white);
        whitePalette.setColor(QPalette::BrightText, Qt::white);
        whitePalette.setColor(QPalette::Link, QColor(42, 130, 218));
        whitePalette.setColor(QPalette::Highlight, Qt::white);
        whitePalette.setColor(QPalette::HighlightedText, Qt::black);
        qApp->setPalette(whitePalette);
        this->SetColor("white", "#e7e7e7", "#404040");
        //this->SetProgressBarColor("#e7e7e7", "#9ace6a");
        this->on_interfaceButton_clicked();
    }
    else if (theme == themes::dark) {
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
        darkPalette.setColor(QPalette::AlternateBase, QColor(25, 25, 25));
        darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::BrightText, Qt::red);
        darkPalette.setColor(QPalette::Highlight, QColor("#9ace6a"));
        darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        qApp->setPalette(darkPalette);
        this->SetColor("rgb(25, 25, 25)", "#292929", "#fff");
        //this->SetProgressBarColor("#000", "#9ace6a");
        this->on_interfaceButton_clicked();
    }
    else if (theme == themes::blue) {
        QPalette bluePalette;
        bluePalette.setColor(QPalette::Window, QColor(86, 115, 197));
        bluePalette.setColor(QPalette::WindowText, QColor(183, 196, 238));
        bluePalette.setColor(QPalette::Base, QColor(183, 196, 238));
        bluePalette.setColor(QPalette::AlternateBase, Qt::black);
        bluePalette.setColor(QPalette::ToolTipBase, Qt::white);
        bluePalette.setColor(QPalette::ToolTipText, Qt::white);
        bluePalette.setColor(QPalette::Text, QColor("#2e4585"));
        bluePalette.setColor(QPalette::Button, QColor(86, 115, 197));
        bluePalette.setColor(QPalette::ButtonText, Qt::white);
        bluePalette.setColor(QPalette::BrightText, Qt::white);
        bluePalette.setColor(QPalette::Link, QColor(42, 130, 218));
        bluePalette.setColor(QPalette::Highlight, Qt::blue);
        bluePalette.setColor(QPalette::HighlightedText, Qt::black);
        qApp->setPalette(bluePalette);
        this->SetColor("rgb(183, 196, 238)", "#7b92d1", "#fff");
        //this->SetProgressBarColor("#8da1e3", "rgb(86, 115, 197)");
        this->on_interfaceButton_clicked();
    }
    else if (theme == themes::green) {
        QPalette greenPalette;
        greenPalette.setColor(QPalette::Window, QColor(100, 191, 141));
        greenPalette.setColor(QPalette::WindowText, QColor(183, 247, 212));
        greenPalette.setColor(QPalette::Base, QColor(183, 247, 212));
        greenPalette.setColor(QPalette::AlternateBase, Qt::black);
        greenPalette.setColor(QPalette::ToolTipBase, Qt::white);
        greenPalette.setColor(QPalette::ToolTipText, Qt::white);
        greenPalette.setColor(QPalette::Text, QColor(55, 135, 90));
        greenPalette.setColor(QPalette::Button, QColor(100, 191, 141));
        greenPalette.setColor(QPalette::ButtonText, Qt::white);
        greenPalette.setColor(QPalette::BrightText, Qt::red);
        greenPalette.setColor(QPalette::Highlight, QColor("#37875a")); // Qt::white
        greenPalette.setColor(QPalette::HighlightedText, Qt::white); // QColor("#286141")
        qApp->setPalette(greenPalette);
        this->SetColor("#46ab73", "#37875a", "#fff");
        //this->SetProgressBarColor("#46ab73", "#37875a");
        this->on_interfaceButton_clicked();
    }
    else if (theme == themes::red) {
        QPalette pinkPalette;
        pinkPalette.setColor(QPalette::Window, QColor(248, 126, 115));
        pinkPalette.setColor(QPalette::WindowText, QColor(252, 202, 198));
        pinkPalette.setColor(QPalette::Base, QColor(252, 202, 198));
        pinkPalette.setColor(QPalette::AlternateBase, Qt::black);
        pinkPalette.setColor(QPalette::ToolTipBase, Qt::white);
        pinkPalette.setColor(QPalette::ToolTipText, Qt::white);
        pinkPalette.setColor(QPalette::Text, QColor("#cd4a4c"));  // QColor(252, 202, 198)
        pinkPalette.setColor(QPalette::Button, QColor(248, 126, 115)); // QColor(248, 126, 115)
        pinkPalette.setColor(QPalette::ButtonText, Qt::white);
        pinkPalette.setColor(QPalette::BrightText, Qt::white);
        pinkPalette.setColor(QPalette::Highlight, QColor("#e66761"));
        pinkPalette.setColor(QPalette::HighlightedText, Qt::white);
        qApp->setPalette(pinkPalette);
        this->SetColor("rgb(248, 126, 115)", "#e66761", "#fff");
        //this->SetProgressBarColor("#cd4a4c", "#fd7b7c");
        this->on_interfaceButton_clicked();
    }
}


void OptionsWindow::SetProgressBarColor(QString background, QString chunk) {
    auto styleSheet = "QProgressBar { border-radius: 5px; text-align: center; background-color: " + background + ";} QProgressBar::chunk {border-radius: 7px; margin: 4px; background-color: " + chunk + ";}";
    qApp->setStyleSheet(styleSheet);
}


void OptionsWindow::on_redBox_clicked()
{
    setRedChecked();
    emit signalSetTheme(themes::red);
}

void OptionsWindow::OkButton_clicked()
{
    this->hide();
}



