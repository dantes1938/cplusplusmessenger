# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles\\UbuTorrent_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\UbuTorrent_autogen.dir\\ParseCache.txt"
  "UbuTorrent_autogen"
  )
endif()
