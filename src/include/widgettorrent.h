#ifndef WIDGETTORRENT_H
#define WIDGETTORRENT_H

#include <QWidget>
#include "dialogtorrent.h"
#include "torrent.h"
#include <array>

#define WIDGET_WIDTH 208

enum class themes {
    light,
    dark,
    green,
    red,
    blue
};




namespace Ui {
class WidgetTorrent;
}

class WidgetTorrent : public QWidget
{
    Q_OBJECT

public:
    WidgetTorrent(int index, std::shared_ptr<Torrent> torrent, QString torrent_file_path_, QWidget *parent = nullptr);
    ~WidgetTorrent();

    void setSize(unsigned long long size);
    void setTorrentName(QString path);
    std::shared_ptr<Torrent> getTorrent();
    


     //доделать

public slots:
    void setProgress();
    void setPeersSeeds();
    void setUpRate();
    void setDownRate();
    void pauseClicked();
    void setStatus();
    void resizes();

signals:
    void signalChangeStatus(size_t index, TorrentStatus status);
    void signalOpen(size_t index);
    void signalShowInFolder(size_t index);
    void signalCopyMagnet(size_t index);
    void signalPlace(size_t index, enum Places place);
    void signalSharingSpeed(size_t index, int speed);
    void signalReceivingSpeed(size_t index, int speed);

private slots:
    void slotOpen();
    void slotShowInFolder();
    void slotCopyMagnet();
    void slotPlace(enum Places place);
    void slotSharingSpeed(int speed);
    void slotReceivingSpeed(int speed);
    void slotDelete();
    

    void on_startStopButton_clicked();
    void on_settingsButton_clicked();

public:
    void setTheme(themes theme_);
    void setPauseIcon(themes theme_);
    void setStartIcon(themes theme_);
    void setStartPauseIcon();
    void setOptionsIcon(themes theme_);
    void setIcons();
    const QString torrentFilePath;
    std::pair<int, int> row_column;

private:

    themes widget_theme = themes::light;
    Ui::WidgetTorrent *ui;
    size_t index = 0;
    unsigned long long size;
    QString name = 0;
    qreal percent = 0.0;
    libtorrent::torrent_status::state_t status = libtorrent::torrent_status::checking_files;
    size_t peers = 0;
    size_t seeds = 0;
    unsigned long long bitrate = 0;
    unsigned long long downrate = 0;
    DialogTorrent* dialogTorrent = nullptr;
    std::shared_ptr<Torrent> torrent;
    int64_t allTimeLoaded = -1;
    QString convert(unsigned long long number);
    void DeleteTorrent(unsigned long long index);
};

#endif // WIDGETTORRENT_H
