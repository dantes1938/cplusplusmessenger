#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "widgettorrent.h"
#include "generalwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void signalSize(unsigned long long size);
    void signalTorrentNum(size_t index);
    void signalProgress(size_t percent);
    void signalStatus(QString status);
    void signalPeersSeeds(size_t peers, size_t seeds);
    void signalUpRate(unsigned long long bitrate);
    void signalDownRate(unsigned long long downrate);
    //void signalConvert(unsigned long long number);

private slots:
    void on_pushButton_clicked();

private:
    void saveTorrents(GeneralWindow* generalWindow);
    void loadTorrents(GeneralWindow* generalWindow);

private:
    Ui::MainWindow *ui;
    WidgetTorrent *widgetTorrent;
    int count = 0;
    GeneralWindow *generalWindow;
    int peers = 0;
    int seeds = 0;
    int UpRate = 0;
    int DownRate = 0;
    QString status = "";
};
#endif // MAINWINDOW_H
