#ifndef GENERALWINDOW_H
#define GENERALWINDOW_H

#include <QWidget>
#include <vector>
#include <utility>
#include "widgettorrent.h"
#include "torrent.h"
#include "libtorrent/session.hpp"
#include <QString>
#include <string>
#include <qfiledialog.h>
#include <QDebug>
#include <QHeaderView>
#include <iostream>
#include <QTimer>
#include <QApplication>
#include <algorithm>
#include <QScrollArea>
#include <QGridLayout>
#include <QLabel>
#include "Save.h"
#include "addmagnet.h"
#include "optionswindow.h"

namespace Ui {
class GeneralWindow;
}


class GeneralWindow : public QWidget
{
    Q_OBJECT

public:
    GeneralWindow(std::shared_ptr<SessionHandler> ses, QWidget* parrent = nullptr);
    ~GeneralWindow();


signals:
    void signalAddedTorrent(size_t index);
    void signalChangeStatus(size_t index, TorrentStatus status);
    void signalOpen(size_t index);
    void signalShowInFolder(size_t index);
    void signalCopyMagnet(size_t index);
    void signalPlace(size_t index, enum Places place);
    void signalSharingSpeed(size_t index, int speed);
    void signalReceivingSpeed(size_t index, int speed);
    void signalNightMode();

public slots:
    void slotSize(size_t index, unsigned long long size);
    void slotAddMagnetLink(QString link);
    /*
    void slotProgress(size_t index, size_t percent);
    void slotStatus(size_t index, TorrentStatus status);  //доделать (удаление)
    void slotPeersSeeds(size_t index, size_t peers, size_t seeds);
    void slotUpRate(size_t index, unsigned long long bitrate);
    void slotDownRate(size_t index, unsigned long long downrate);
    */

private slots:
    void on_addTorrentButton_clicked();
    void on_nightModeButton_clicked();
    void on_buttonDeleteAll_clicked();
    void on_buttonAddMagnetUrl_clicked();
    void on_optionsButton_clicked();

    void slotAddMagnetClosed();
    void slotChangeStatus(size_t index, TorrentStatus status); //доделать
    void slotOpen(size_t index);
    void slotShowInFolder(size_t index);
    void slotCopyMagnet(size_t index);
    void slotPlace(size_t index, enum Places place);
    void slotSharingSpeed(size_t index, int speed);
    void slotReceivingSpeed(size_t index, int speed);

public:
    themes theme = themes::light;
    std::shared_ptr<SessionHandler> session;
    std::vector<std::pair<WidgetTorrent*, int>> widgetTorrents;
    void addTorrent(QString path, torrentType type = torrentType::byPath, bool inProgram = true);
    void reAddTorrents();
    void setTheme(themes new_theme);

    
    void enableReceiveLimit(int speed);
    void disableReceiveLimit();
    void enableShareLimit(int speed);
    void disableShareLimit();
    void changeSavePath(QString path);

private:
   
    AddMagnet* addMagnetWindow = nullptr;
    OptionsWindow* optionsWindow = nullptr;
    QGridLayout* torrentsGridLayout;
    int32_t adding_index = 0;
    Ui::GeneralWindow *ui;
    int lastAddedNumber = -1;
    size_t torrentIdx = 0;
    QString torrent_file_path;
    
};

#endif // GENERALWINDOW_H
