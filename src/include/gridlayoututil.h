#pragma once
#include <QGridLayout>
#include <QWidget>


class GridLayoutUtil {

public:
	static void removeRow(QGridLayout* layout, int row, bool deleteWidgets = true);
	static void removeColumn(QGridLayout* layout, int column, bool deleteWidgets = true);
	static void removeCell(QGridLayout* layout, int row, int column, bool deleteWidgets = true);
private:
	static void remove(QGridLayout* layout, int row, int column, bool deleteWidgets);
	static void deleteChildWidgets(QLayoutItem* item);



};