#pragma once
#include <iostream>
#include <string>
#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/torrent_status.hpp>
#include <libtorrent/error_code.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/alert_types.hpp>
#include <memory>
#include "torrent_info.h"

enum class torrentType {
    byPath,
    byLink
};

class Torrent {
public:
    Torrent(lt::torrent_handle th);
    Torrent();
    lt::torrent_handle GetHandle();
    Info GetStats();
    torrentType type;
private:
    lt::torrent_handle torrent_handle;
    
};

struct TorrentList {
    std::vector<std::shared_ptr<Torrent>> torrents;
    void addTorrent(lt::torrent_handle torrent_handle);
};

class SessionHandler {
public:
    SessionHandler(lt::session_params old_session);
    Torrent AddTorrentByPath(const std::string& path_to_torrent);
    Torrent AddTorrentByLink(const std::string& link_to_torrent);
    void PopAlerts(std::vector<lt::alert*> *alerts);
    void PostTorrentUpdates();
    std::shared_ptr<lt::session> getLtSession();

public:
    std::string save_path = ".";

private:
    std::shared_ptr<lt::session> session;
    
};
