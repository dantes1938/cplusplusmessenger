#pragma once
#include <string>
#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <fstream>
#include "QString"

#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/torrent_info.hpp>
#include <libtorrent/session_stats.hpp>
#include <libtorrent/session_params.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/alert.hpp>
//#include <libtorrent/alert_manager.hpp>
#include <libtorrent/write_resume_data.hpp>
#include <libtorrent/read_resume_data.hpp>


void save(std::shared_ptr<lt::session> ses);

lt::session_params loadSessionParams();