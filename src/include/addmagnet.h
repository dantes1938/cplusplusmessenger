#ifndef ADDMAGNET_H
#define ADDMAGNET_H
#include <QDialog>
#include <QString>
#include <QCloseEvent>
namespace Ui {
	class AddMagnet;
}

class AddMagnet : public QDialog
{
	Q_OBJECT

public:
	AddMagnet(QDialog* parent = nullptr);
	~AddMagnet();


signals:
	void addMagnetTorrent(QString link);
	void magnetAddClosed();

private slots:
	void on_addTorrent_clicked();

private:
	Ui::AddMagnet* ui;
	void closeEvent(QCloseEvent* event);

};






#endif
