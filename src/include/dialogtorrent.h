#ifndef DIALOGTORRENT_H
#define DIALOGTORRENT_H

#include <QWidget>
#include <memory>
#include <torrent.h>
#include <QDesktopServices>
#include <qclipboard.h>
namespace Ui {
class DialogTorrent;
}

enum Places {toUp, toDown, toBegin, toEnd};
enum TorrentStatus {downloading, done, paused, deleted};

class DialogTorrent : public QWidget
{
    Q_OBJECT

public:
    DialogTorrent(std::shared_ptr<Torrent> torrent, QWidget *parent = nullptr);
    ~DialogTorrent();

private slots:
    void on_openButton_clicked();
    void on_showInFolderButton_clicked();
    void on_copyMagnetButton_clicked();
    void on_changePlaceBox_currentIndexChanged(int index);
    void on_sharingSpeedBox_valueChanged(int arg1);
    void on_comboBoxShare_currentIndexChanged(int index);
    void on_receivingSpeedBox_valueChanged(int arg1);
    void on_comboBoxReceive_currentIndexChanged(int index);
    void on_deleteButton_clicked();

signals:
    void signalOpen();
    void signalShowInFolder();
    void signalCopyMagnet();
    void signalPlace(enum Places place);
    void signalSharingSpeed(int speed);
    void signalReceivingSpeed(int speed);
    void signalDelete();

private:
    Ui::DialogTorrent *ui;
    std::shared_ptr<Torrent> torrent;
};

#endif // DIALOGTORRENT_H
