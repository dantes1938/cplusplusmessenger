#pragma once
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/torrent_status.hpp>

class Info {
public:
	Info(lt::torrent_handle th);
	// ��������� ����������
	void Update();
	// ���������� ���-�� �����
	int GetCountPeers();
	// ���������� ���-�� �����
	int GetCountSeeds();
	// �������� � ��������� [0, 1], �������������� ��� ���������� ������� ������ ��������. ��� ����� ���� �������� ������ ��� ��������.
	float GetProgress();
	// �������� ���� ��� ���������� ������ � �������
	int GetUploadRate();
	// �������� ���� ��� ���������� ������ � �������
	int GetDownloadRate();
	// ���������� true, ���� ��� ����� �������� ���� ���������
	bool IsSeeding();
	// ���������� true, ���� ������� ��� ������������� ��-�� ������
	bool Error();
	// ���������� ��������� �� ������, ���� ������� ��� ������������� ��-�� ������
	std::string GetErrorMessage();
	// ���������� ��� ������, ���� ������� ��� ������������� ��-�� ������
	int GetErrorCode();
	// ����� ���������� ������ ��� �������� ��� ����� ��������
	std::int64_t GetTotal();
	// ����� ���������� ������ �����, ������� ��� ���� ���������. ��� ��� �� ����������� ������ ���� ���� ��������� �� ����� ���� ������
	std::int64_t GetTotalDone();
	// ���������� ������� ������ ��������
	std::string GetState();
	// ���������� ��� ������� �����
	std::string GetName();

private:
	lt::torrent_handle torrent_handle;
	int count_peers;
	int count_seeds;
	float progress;
	int upload_rate;
	int download_rate;
	bool is_seeding;
	lt::error_code error;
	lt::torrent_status::state_t state;
	std::int64_t total_done;
	std::int64_t total;
	std::string name;
};