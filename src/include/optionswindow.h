#ifndef OPTIONSWINDOW_H
#define OPTIONSWINDOW_H

#include <QMainWindow>
#include "widgettorrent.h"
#include <QDir>

namespace Ui { class OptionsWindow; }

class OptionsWindow : public QMainWindow
{
    Q_OBJECT

public:
    OptionsWindow(QMainWindow* parent = nullptr);
    ~OptionsWindow();
    void setThemeChecked(themes theme);
    void setSavePath(QString path);
    void SetColor(QString background, QString hover, QString text_indicator);
    void setThemes(themes theme);
    void SetProgressBarColor(QString background, QString chunk);
   
signals:

    void enableReceiveLimit(int speed);
    void disableReceiveLimit();
    void enableShareLimit(int speed);
    void disableShareLimit();
    void signalSetTheme(themes theme);
    void savePathChanged(QString path);


public slots:
    void on_interfaceButton_clicked();

    void on_infoButton_clicked();

    void on_lightBox_clicked();

    void on_darkBox_clicked();

    void on_blueBox_clicked();

    void on_greenBox_clicked();

private slots:

   

    void on_foldersButton_clicked();

    void on_speedButton_clicked();

    void on_pathButton_clicked();

    void on_changeButton_clicked();

    

    void on_redBox_clicked();

    void on_sharingSpeedBox_valueChanged(int arg1);

    void on_receivingSpeedBox_valueChanged(int arg1);

    void on_speedBox1_toggled(bool state);

    void on_speedBox2_toggled(bool state);

    void OkButton_clicked();

    



private:
    QString background;
    QString hover;
    QString text_indicator;

    themes offlineCombo = themes::light;
    Ui::OptionsWindow* ui;
    void setLightChecked();
    void setDarkChecked();
    void setBlueChecked();
    void setRedChecked();
    void setGreenChecked();
};

#endif 
